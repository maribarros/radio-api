# coding=utf-8

from django.test import TestCase
from model_mommy import mommy
from apps.api.models import Radio, Show, Host


class TestRadio(TestCase):
	def setUp(self):
		self.radio = mommy.make(Radio, name='Radio')

	def test_radio_creation(self):
		self.assertTrue(isinstance(self.radio, Radio))
		self.assertEqual(self.radio.__str__(), self.radio.name)


class TestHost(TestCase):
	def setUp(self):
		self.host = mommy.make(Host, name='Apresentador')

	def test_host_creation(self):
		self.assertTrue(isinstance(self.host, Host))
		self.assertEqual(self.host.__str__(), self.host.name)

class TestShow(TestCase):
	def setUp(self):
		self.show = mommy.make(Show, name='Programas')

	def test_show_creation(self):
		self.assertTrue(isinstance(self.show, Show))
		self.assertEqual(self.show.__str__(), self.show.name)