from rest_framework import viewsets, status



from .serializers import RadioSerializer, HostSerializer, ShowSerializer
from . import models
from rest_framework import viewsets
from rest_framework.response import Response



class RadioViewSet(viewsets.ModelViewSet):
    queryset = models.Radio.objects.all()
    serializer_class = RadioSerializer

    def get(self, request, format=None):
        pass

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response({"messagem": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


class HostViewSet(viewsets.ModelViewSet):
    queryset = models.Host.objects.all()
    serializer_class = HostSerializer
    def get(self, request, format=None):
        pass

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response({"messagem": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

class ShowViewSet(viewsets.ModelViewSet):
    queryset = models.Show.objects.all()
    serializer_class = ShowSerializer

    def get(self, request, format=None):
        pass

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response({"messagem": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
