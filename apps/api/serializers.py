from rest_framework import fields, serializers
from . import models


class RadioSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Radio
        fields = ('name',)


class HostSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Host
        fields = ('name',)


class ShowSerializer(serializers.ModelSerializer):
    radio = RadioSerializer()
    host = HostSerializer()

    class Meta:
        model = models.Show
        fields = (
            'radio', 'name', 'start', 'end', 'day',
            'is_soccer', 'description', 'host',
        )