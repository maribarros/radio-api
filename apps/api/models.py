from django.db import models


class Radio(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Host(models.Model):
    name = models.CharField(max_length=300)

    def __str__(self):
        return self.name


class Show(models.Model):
    WEEK_DAYS = (
        ('dom', 'sunday'),
        ('seg', 'monday'),
        ('ter', 'tuesday'),
        ('qua', 'wednesday'),
        ('qui', 'thursday'),
        ('sex', 'friday'),
        ('sab', 'saturday'),
    )
    name = models.CharField('name', max_length=200)
    start = models.TimeField('start')
    end = models.TimeField('end')
    day = models.CharField('day', max_length=3, choices=WEEK_DAYS)
    is_soccer = models.BooleanField('is soccer', default=False)
    description = models.TextField('description')
    host = models.ForeignKey(
        'Host',
        verbose_name='host',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    radio = models.ForeignKey(
        'Radio',
        verbose_name='radio',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name
