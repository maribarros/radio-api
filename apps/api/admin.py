from django.contrib import admin

from . import models


admin.site.register(models.Radio)
admin.site.register(models.Host)
admin.site.register(models.Show)
