from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'radio', views.RadioViewSet, base_name='radio')
router.register(r'host', views.HostViewSet)
router.register(r'show', views.ShowViewSet)

urlpatterns = [
    path(r'', include(router.urls)),
]
