API para o programa que esta no ar, a lista de programa de cada radio e a grade de programação de cada rádio.

## Como desenvolver?

1- Clone o repositorio
2- Crie um virtualenv com Python 3.5
3- Ative o virtualenv
4- Instale as dependeincias
5- Configure a instancia com o .env
6- Execute os testes

```
git clone https://maribarros@bitbucket.org/maribarros/api-radio.git
cd api-radio
virtualenv .venv
source .venv/bin/activate
pip3 install -r requirements-dev.txt
cp contrib/env-sample .env
python manage.py test
```

## Como usar a API

1- "radio": "http://127.0.0.1:8000/radio/", São as radios CBN, Radio Globo e BHFM
    "host": "http://127.0.0.1:8000/host/", Apresentadores de cada programa, referente a cada rádio
    "show": "http://127.0.0.1:8000/show/", Toda a programação das rádios

Para salvar os dados do CSV no banco, digite:

```
python create_data.py
```

