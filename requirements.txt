dj-database-url==0.5.0
Django==2.1.3
django-extensions==2.1.3
django-test-without-migrations==0.6
djangorestframework==3.9.0
python-decouple==3.1
