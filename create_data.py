import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()

import pandas as pd
from apps.api.models import Host, Show, Radio

def create_host(filename):
    '''
    Criando o Host
    '''
    # Lendo o CSV com Pandas, criando um DataFrame.
    df = pd.read_csv(filename).fillna('')
    #import ipdb; ipdb.set_trace()
    # Com o set pegamos apenas um Host de cada.
    hosts = list(set(df['Host'].tolist()))
    # Tirando string vazia.
    if '' in hosts:
        hosts.remove('')

    aux = []
    #Host.objects.all().delete()
    for host in hosts:
        # Criando a instancia do Host
        obj = Host(name=host)
        aux.append(obj)
    # O metodo bulk_create eh mais rapido
    Host.objects.bulk_create(aux)

def create_show(filename):
    df = pd.read_csv(filename).fillna('')
    items = df.T.apply(dict).tolist()
    
    aux = []
    #Show.objects.all().delete()
    for item in items:
        # Separando os times
        _split = items[0]['Time'].split(' às ')
        start = _split[0]
        end = _split[1]
        day = item['Day']
        programa = item['Programas']
        # Buscando o nome da Radio no bd
        radio = Radio.objects.filter(name='Radio Globo').first()
        host = item.get('Host')
        _host = ''
        if host:
            # Buscando o nome do Host no bd
            _host = Host.objects.filter(name=host).first()
            obj = Show(
                name=programa,
                start=start,
                end=end,
                day=day,
                description='Descriçao',
                host=_host,
                radio=radio,
            )
        else:
            obj = Show(
                name=programa,
                start=start,
                end=end,
                day=day,
                description='Descriçao',
                radio=radio,
            )
        aux.append(obj)
    Show.objects.bulk_create(aux)
    

filename = 'radio-globo.csv'
create_host(filename)
create_show(filename)
